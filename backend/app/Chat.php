<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    protected $table = 'chats';
    protected $guarded = ['id'];
    public $timestamps = true;

    public function source()
    {
    	return $this->belongsTo('App\User', 'source_id');
    }

    public function target()
    {
    	return $this->belongsTo('App\User', 'target_id');
    }
}
