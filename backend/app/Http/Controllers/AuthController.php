<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use JWTAuth;
use App\User;
use Auth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $username = $request->input('username');
        $password = $request->input('password');
        $user = User::where('username', $username)->first();
        if($user) {
            try {
                if (!$token = JWTAuth::attempt(['username' => $username, 'password' => $password], $user->toArray())) {
                    return response()->json([
                        'succeed' => false,
                        'data' => 'Invalid username / password',
                        ], 400);
                }
            } catch (JWTException $e) {
                return response()->json([
                    'succeed' => false,
                    'data' => 'could not create token'
                    ], 502);
            }
            $user->online = 1;
            $user->save();
            return response()->json(compact('token'));
        }
        else {
            return response()->json([
                'succeed' => false,
                'data' => 'Invalid username / password',
                ], 400);
        }
    }

    public function logout()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $user->online = 0;
        $user->save();
        return response()->json([
            'succeed' => true,
            'data' => 'Logout success'
        ]);
    }

    public function register(Request $request) {
        $user = User::create([
            'username' => $request->input('username'),
            'password' => Hash::make($request->input('password')),
            'name' => $request->input('name'),
            'email' => $request->input('email'),
        ]);
        if($user) {
            try {
                if (!$token = JWTAuth::attempt(['username' => $request->input('username'), 'password' => $request->input('password')])) {
                    return response()->json([
                        'succeed' => false,
                        'data' => 'Register error',
                        ], 400);
                }
            } catch (JWTException $e) {
                return response()->json([
                    'succeed' => false,
                    'data' => 'could not create token'
                    ], 502);
            }
            return response()->json(compact('token'));
        }
        return response()->json([
            'succeed' => false,
            'data' => 'Register error !',
            ], 400);
    }
}