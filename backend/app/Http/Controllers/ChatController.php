<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use JWTAuth;
use App\Chat;
use App\User;

class ChatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        $chat = Chat::create($request->all());
        if($chat) {
            return response()->json();
        }
        else {
            return response()->json([], 404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $source = JWTAuth::parseToken()->authenticate();
        $target = User::find($id);
        $chats = Chat::with('source', 'target')
                    ->where('source_id', $source->id)
                    ->where('target_id', $target->id)
                    ->orWhere('source_id', $target->id)
                    ->where('target_id', $source->id)
                    ->orderBy('created_at', 'asc')
                    ->get();
        $ret = [
            'title' => $target->name,
            'channelName' => 'private:' . $target->id,
            'targetId' => $target->id,
            'messages' => $chats
        ];
        return $ret;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
