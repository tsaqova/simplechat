<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group(['middleware' => ['jwt.auth']], function() {
	Route::resource('chat', 'ChatController');
	Route::resource('user', 'UserController');
	Route::get('auth/logout', 'AuthController@logout');
});

Route::post('auth/register', 'AuthController@register');
Route::post('auth/login', 'AuthController@login');