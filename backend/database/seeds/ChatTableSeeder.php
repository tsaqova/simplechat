<?php

use Illuminate\Database\Seeder;
use App\Chat;

class ChatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$chats = [];

        Chat::truncate();

    	// $chats[] = [
    	// 	'source_id' => 1,
    	// 	'target_id' => 2,
    	// 	'message' => 'Hello World !'
    	// ];

    	// $chats[] = [
    	// 	'source_id' => 2,
    	// 	'target_id' => 1,
    	// 	'message' => 'Hello World too !'
    	// ];

    	// $chats[] = [
    	// 	'source_id' => 1,
    	// 	'target_id' => 3,
    	// 	'message' => 'Hello'
    	// ];

    	// $chats[] = [
    	// 	'source_id' => 3,
    	// 	'target_id' => 1,
    	// 	'message' => 'Hola !'
    	// ];

    	// Chat::insert($chats);
    }
}
