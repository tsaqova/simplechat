<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

    	User::create([
    		'username' => 'tsaqova',
    		'password' => Hash::make('tsaqova'),
    		'name' => 'tsaqova',
    		'email' => 'yusrotsaqova@gmail.com',
            'online' => 0
    	]);

    	User::create([
    		'username' => 'luthfie',
    		'password' => Hash::make('luthfie'),
    		'name' => 'luthfie',
    		'email' => 'luthfie@gmail.com',
            'online' => 0
    	]);

    	User::create([
    		'username' => 'yusro',
    		'password' => Hash::make('yusro'),
    		'name' => 'yusro',
    		'email' => 'yusro@gmail.com',
            'online' => 0
    	]);
    }
}
