module.exports.attach = function (scServer, socket) {
	var userManagement = require('./user_management');
	var channelHelper = require('../helpers/channel');
	var chatsConfig = require('../config/chats');
	var chatsModel = require('../models/chats');
	var jwt = require('jsonwebtoken');

	var verifyLogin = function(signedToken, respond) {
		jwt.verify(signedToken, chatsConfig.SECRET_PASSPHRASE, {ignoreExpiration: true}, function(err, decodedToken){
			if (err) {
				respond();
			} else {
				decodedToken.token = signedToken;
				socket.setAuthToken(decodedToken);
      			respond();
			}
		});
	}

	var logoutProcess = function(logoutData, respond) {
		console.log('Logged Out');
        var userID = socket.getAuthToken().sub;
        scServer.global.publish('presence:all', {id: userID, online:0});
		socket.removeAuthToken();
		respond();
	}

	if (!socket.getAuthToken()) {
		// console.log(socket.getAuthToken());
		// Authentication using request authorization
		var cookieToken = socket.request.headers.cookie;

		function getCookie(cookie, cname) {
		    var name = cname + "=";
		    var ca = cookieToken.split(';');
		    for(var i=0; i<ca.length; i++) {
		        var c = ca[i];
		        while (c.charAt(0)==' ') c = c.substring(1);
		        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
		    }
		    return "";
		}

		if (cookieToken !== undefined) {
			var signedToken = getCookie(cookieToken, 'fjbchat_token');
			verifyLogin(signedToken, function(){});
		}
	} else {
		var userID = socket.getAuthToken().sub;
		scServer.global.publish('presence:all', {id: userID, online:1});
		console.log('Online Presence');
	}
	//  Validates if client emitted login
	var validateLogin = function (credentials, respond) {
		if (socket.getAuthToken()) {
			console.log(socket.getAuthToken().username);
			respond(null, userManagement.presenceData);
		} else {

			// Validates if data not exists
			undefinedCredentials = credentials === undefined || credentials === null
			if (undefinedCredentials) {
				// Error Handling
				console.log(socket.getAuthToken().username);
				respond(null,"");
			}
			// Check data from database

			usersModel.login(credentials.username, credentials.password, function(err, rows) {
    			var userRow = rows[0];
				var isValidLogin = userRow;

				// If user exist
				if (isValidLogin) {
					// Register user to online user list
					userManagement.setUser(socket, userRow);

					// Send response of list online users
					responseData = {
						presenceData: userManagement.presenceData,
						channelName: channelHelper.PRIVATE_CHANNEL(userRow.id)
					};
					respond(null, responseData);

					// Inform while new user connected
					scServer.global.publish('presence:all', userRow);
				} else {
					// Response login failed
					respond(401, 'Invalid username or password');
				}
			});
		}
	}
	socket.on('login', verifyLogin);
	socket.on('logout', logoutProcess);
};