module.exports.attach = function (scServer) {
  // SocketCluster middleware for access control
  var getChannelDetails = function (channelName) {
    if (channelName === undefined ) return false;
    var splitChannel = channelName.split(':');
    if (splitChannel.length > 1) {
      var channelData = {
        type: splitChannel[0],
        identifier: splitChannel[1]
      };
      return channelData;
    } else {
        var channelData = {
          type: 'Other',
          identifier: splitChannel[0]
        };
      return channelData;
    }
  };

  var channelHelper = require('../helpers/channel');
  var chatsModel = require('../models/chats');

  scServer.addMiddleware(scServer.MIDDLEWARE_PUBLISH_IN, function (socket, channel, data, next) {
    // Validation to restrict user to send message

    var isAllowedToPublish = true && socket.getAuthToken();
    if (isAllowedToPublish)
      var channelData = getChannelDetails(channel);

      // Insert to database
      console.log('Published');
      chatsModel.create(socket.getAuthToken().sub, channelData.identifier, data.message, socket, function(result){
        console.log(result.statusCode);
        if (result.statusCode == 200) {
          scServer.global.publish(channelHelper.PRIVATE_CHANNEL(data.source.id), data);
          isAllowedToPublish = true;
        } else {
          isAllowedToPublish = false;
        }
      });
    if (isAllowedToPublish) {
      next();
    } else {
      next(socket.id + ' is not allowed to publish channel ' + channel);
    }
  });

  scServer.addMiddleware(scServer.MIDDLEWARE_SUBSCRIBE, function (socket, channel, next) {
    // Validates subscriptions
    var isAllowedToSubscribe = false;

    if (socket.getAuthToken())
    {
      var channelData = getChannelDetails(channel);

      switch(channelData.type) {
        case 'private':
          if (socket.getAuthToken().sub == channelData.identifier)
            isAllowedToSubscribe = true;
          break;
        case 'auction': 
          isAllowedToSubscribe = true;
          break;
        case 'presence':
          isAllowedToSubscribe = true;
          break;
        default:
          isAllowedToSubscribe = false;
          break;
      }

      if (isAllowedToSubscribe) {
        next() // Allow
      } else {
        next(socket.id + ' is not allowed to subscribe to channel ' + channel); // Block
      }
    }
  });
};