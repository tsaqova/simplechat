var chatsConfig = require('../config/chats');

var userManagement = {
  presenceData: {},
  setUser: function(socket, userData) {
			socket.setAuthToken(userData);
	  	this.presenceData[userData.id] = {
	      id: userData.id,
	      name: userData.name,
	      username: userData.username,
	      onlineStatus: chatsConfig.CONNECTED_STATUS
	  	};
      console.log(presenceData);
  },
  getPresence: function (userID) {
  	if (this.presenceData.hasOwnProperty(userID))
  		return this.presenceData[userID];
  	else
  		return null;
  },
  removeUser: function (userID) {
  	delete this.presenceData[userID];
  }
};

module.exports = userManagement;