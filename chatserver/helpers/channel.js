var CryptoJS = require('crypto-js/aes');
var chatsConfig = require('../config/chats');

var channelHelpers = {
	PRIVATE_CHANNEL: function(username) {
		var channelName = "private:" + username;
		return channelName;
	},
	ENCRYPT_CHANNEL: function(channelName) {
		var result = CryptoJS.encrypt(channelName, chatsConfig.SECRET_PASSPHRASE).toString();
		return result;
	},
	ENCRYPT_PRIVATE_CHANNEL: function(username ) {
		return this.ENCRYPT_PRIVATE_CHANNEL(this.PRIVATE_CHANNEL(username));
	},
	DECRYPT_CHANNEL: function(encryptedChannel) {
		var decryptedChannel = CryptoJS.decrypt(encryptedChannel, chatsConfig.SECRET_PASSPHRASE).toString(CryptoJS.enc.utf8);
		return decryptedChannel;
	}
};

module.exports = channelHelpers;