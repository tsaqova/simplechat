var errorHelper = {
	UNAUTHORIZED: function(message) {
		if (message === undefined) {
			return 'Unauthorized';
		} else {
			return 'You are not allowed to ' + message;
		}
	}
};

module.exports = errorHelper;