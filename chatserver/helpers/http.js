var queryString = require('querystring');
var jwt = require('jsonwebtoken');
var http = require('http');
var chatsConfig = require('../config/chats');

var httpHelper = {
	socket: '',
	renderOptions: function (path, method, data) {

		var token = this.socket.getAuthToken().token;
		var options = {
		host: chatsConfig.BACKEND.URL,
		port: chatsConfig.BACKEND.PORT,
		path: chatsConfig.BACKEND.PORT + path,
		method: method, 
		headers: {
	        'Content-Type': 'application/x-www-form-urlencoded',
	        'Content-Length': Buffer.byteLength(data),
	        'Authorization' : "Bearer " + token
		}};

		return options;
	},
	sendGET: function(path, data, callback) {
		this.send(path, 'GET', data, callback);
	},
	sendPOST: function(path, data, callback) {
		this.send(path, 'POST', data, callback);
	},
	send: function (path, method, data, callback) {
		var serializedData = queryString.stringify(data);
		var options = this.renderOptions(path, method, serializedData);
		
		var req = http.request(options, callback);

		req.on('error', function(e){
			console.log('Problem while request: ' + e.message);
		})

		req.write(serializedData);
		req.end();
	}
	
};

module.exports = httpHelper;