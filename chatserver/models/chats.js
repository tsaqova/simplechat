var http = require('../helpers/http');
var chatsModel = {
	create: function(source, target, message, socket, callback) {
				var userData = {
					source_id: source,
					target_id: target,
					message: message,
				};
				http.socket = socket;
				http.sendPOST('/chat', userData, callback);
			},
	presence: function(userID, status, socket, callback) {
				var presenceData = {
					online: status
				}
				http.socket = socket;
				http.sendPOST('/user/' + userID, presenceData, callback);
	}
};

module.exports = chatsModel;