// External Application Modules
var authentication = require('./app_modules/authentication');
var chatMechanism = require('./app_modules/chat_mechanism');
var chatsModel = require('./models/chats');
// var userRequest = require('./app_modules/user_request');

module.exports.run = function (worker) {
  console.log('   >> Worker PID:', process.pid);
  var httpServer = worker.httpServer;
  var scServer = worker.scServer;

  // Middleware for Chat Mechanism
  chatMechanism.attach(scServer);

  scServer.on('connection', function (socket) {
    // Authentication Logic
    authentication.attach(scServer, socket);
    // Handle while user disconnect
    socket.on('disconnect', function(){
      if (socket.getAuthToken()) {
        console.log('Disconnected');
        var userID = socket.getAuthToken().sub;
        scServer.global.publish('presence:all', {id: userID, online:0});
      }
    });
  });
};