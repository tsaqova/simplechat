'use strict';

/**
 * @ngdoc overview
 * @name fjbchatApp
 * @description
 * # fjbchatApp
 *
 * Main module of the application.
 */
 angular
 .module('fjbchatApp', [
  'ngAnimate',
  'ngCookies',
  'ngResource',
  'ngRoute',
  'ngSanitize',
  'ngTouch',
  'ngStorage',
  'satellizer'
  ])
 .constant('Constant', {
    socketServerHost: '10.175.154.93',
    socketServerPort: 8000,
    serverHost: '10.175.154.94/fjbchat-backend/public/'
  })
 .config(['$routeProvider', '$httpProvider', '$authProvider', 'Constant', function ($routeProvider, $httpProvider, $authProvider, Constant) {
  $authProvider.baseUrl = 'http://' + Constant.serverHost;
  $authProvider.loginOnSignup = true;
  $authProvider.signupRedirect = '/';
  $authProvider.logoutRedirect = '/login';
  $authProvider.loginUrl = '/auth/login';
  $authProvider.signupUrl = '/auth/register';
  $authProvider.loginRoute = '/login';
  $authProvider.signupRoute = '/register';
  $authProvider.tokenPrefix = 'fjbchat';
  $routeProvider.when('/', {
    templateUrl: 'views/main.html',
    controller: 'MainCtrl',
    controllerAs: 'main'
  })
  .when('/login', {
    templateUrl: 'views/login.html',
    controller: 'LoginCtrl',
    controllerAs: 'login'
  })
  .when('/register', {
    templateUrl: 'views/register.html',
    controller: 'RegisterCtrl',
    controllerAs: 'register'
  })
  .when('/debug', {
    templateUrl: 'views/debug.html',
    controller: 'DebugCtrl',
    controllerAs: 'debug'
  })
  .otherwise({
    redirectTo: '/'
  });
}])
.run(['$location', '$rootScope', '$timeout', '$auth', function($location, $rootScope, $timeout, $auth) {
  $rootScope.layout = {};
  $rootScope.layout.loading = false;
  $rootScope.$on('$routeChangeSuccess', function () {
    $timeout(function(){
      $rootScope.layout.loading = false;
    }, 650);
  });
  $rootScope.$on('$routeChangeStart', function () {
      $timeout(function(){
        $rootScope.layout.loading = true;
      });
    });
  $rootScope.$on('$routeChangeError', function () {
    $rootScope.layout.loading = false;
  });
  $rootScope.$on('$routeChangeStart', function (event, next, current) {
    if(next.templateUrl !== 'views/login.html' && next.templateUrl !== 'views/register.html' && !$auth.isAuthenticated()) {
      event.preventDefault();
      $location.path('/login');
    }
    $timeout(function(){
      $rootScope.layout.loading = true;
    });
  });
}]);