'use strict';

/**
 * @ngdoc function
 * @name fjbchatApp.controller:DebugCtrl
 * @description
 * # DebugCtrl
 * Controller of the fjbchatApp
 */
angular.module('fjbchatApp')
  .controller('DebugCtrl', ['$scope', '$auth', 'SocketCluster', function ($scope, $auth) {
  	console.log('debug');
    console.log($auth.getPayload());
  }]);
