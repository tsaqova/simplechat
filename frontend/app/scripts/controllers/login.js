'use strict';

/**
 * @ngdoc function
 * @name fjbchatApp.controller:LoginCtrl
 * @description
 * # LoginCtrl
 * Controller of the fjbchatApp
 */
 angular.module('fjbchatApp')
 .controller('LoginCtrl', ['$scope', 'Auth', function ($scope, Auth) {
 	$scope.submitLogin = function() {
 		var credientials = {
 			username: $scope.username,
 			password: $scope.password
 		};
 		Auth.login(credientials);
 	};
 }]);