'use strict';

/**
 * @ngdoc function
 * @name fjbchatApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the fjbchatApp
 */
 angular.module('fjbchatApp')
 .controller('MainCtrl', ['$scope', '$location', 'Auth', 'User', 'SocketCluster', function ($scope, $location, Auth, User, SocketCluster) {

  $scope.chatBoxes = {};
  $scope.users = User.get();

  $scope.initChatbox = function(keyID) {
    SocketCluster.getChatHistory(keyID, function(res) {
      var data = res.data;
      $scope.chatBoxes[keyID] = {
        title: data.title,
        channelName: data.channelName,
        targetId: data.targetId,
        chats: data.messages,
        display: true,
        chatContent: ''
      };
    }, function(res) {
      console.log(res);
    });
  };

  SocketCluster.connect(function(chat) {
    var key = chat.source.id;
    if(chat.source.id === Auth.user().id) {
      key = chat.target.id;
    }
    if(!$scope.chatBoxes[key]) {
      $scope.chatBoxes[key] = $scope.initChatbox(key);
    }
    else {
      $scope.chatBoxes[key].chats.push(chat);
    }
    console.log('receive message');
    console.log(chat);
    $scope.$apply();
  }, function(user) {
    console.log('receive presence');
    console.log(user);
    $scope.users[user.id].online = user.online;
    $scope.$apply();
  });

  $scope.openChatBox = function(user) {
    if(!(user.id in $scope.chatBoxes)) {
      $scope.chatBoxes[user.id] = $scope.initChatbox(user.id);
    }
  };

  $scope.closeChatBox = function(key) {
    delete $scope.chatBoxes[key];
  };

  $scope.toggleChatBox = function(chatBox) {
    chatBox.display = !chatBox.display;
  };

  $scope.sendChat = function(chatBox) {
    var chat = {
      source: {
        id: Auth.user().id,
        name: Auth.user().name
      },
      target: {
        id: chatBox.targetId,
        name: chatBox.title
      },
      message: chatBox.chatContent
    };
    SocketCluster.sendChat(chatBox.channelName, chat, function() {
      chatBox.chatContent = '';
    });
  };

  $scope.logout = function() {
    SocketCluster.logout();
    Auth.logout();
  };

}]);