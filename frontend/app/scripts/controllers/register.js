'use strict';

/**
 * @ngdoc function
 * @name fjbchatApp.controller:RegisterCtrl
 * @description
 * # RegisterCtrl
 * Controller of the fjbchatApp
 */
angular.module('fjbchatApp')
  .controller('RegisterCtrl', ['$location', '$scope', 'Auth', function ($location, $scope, Auth) {
  	$scope.submitRegister = function() {
  		Auth.register({
  			name: $scope.name,
  			email: $scope.email,
  			username: $scope.username,
  			password: $scope.password
  		});
  	};
  }]);
