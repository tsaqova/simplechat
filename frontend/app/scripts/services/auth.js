'use strict';

/**
 * @ngdoc service
 * @name fjbchatApp.auth
 * @description
 * # auth
 * Service in the fjbchatApp.
 */
angular.module('fjbchatApp')
  .service('Auth', ['$auth', 'Backend', function ($auth, Backend) {
    return {
    	user: function() {
    		return $auth.getPayload();
    	},
      register: function(credientials) {
        $auth.signup(credientials)
        .then(function(res) {
          console.log(res);
        }, function(res) {
          Materialize.toast(res.data.data, 4000);
        });
      },
      login: function(credientials) {
        $auth.login(credientials)
        .then(function(res) {
          console.log('login auth');
          console.log(res);
        }, function(res) {
          Materialize.toast(res.data.data, 4000);
        });
      },
      logout: function() {
        Backend.get('auth/logout', function() {
          console.log('logout gan');
          $auth.logout();
        });
      },
      getToken: function() {
        return $auth.getToken();
      }
    };
  }]);
