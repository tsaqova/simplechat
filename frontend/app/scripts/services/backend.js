'use strict';

/**
 * @ngdoc service
 * @name fjbchatApp.backend
 * @description
 * # backend
 * Service in the fjbchatApp.
 */
angular.module('fjbchatApp')
  .service('Backend', ['$http', 'Constant', function ($http, Constant) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var host = Constant.serverHost;
    var backendUrl = function(req) {
        return "http://" + host + req;
    };
    return {
    	get: function(reqUrl, success, error) {
    		$http.get(backendUrl(reqUrl)).then(success, error);
    	},
    	post: function(reqUrl, data, success, error) {
			$http.post(backendUrl(reqUrl), data).then(success, error);
    	},
        url: function(reqUrl) {
            return backendUrl(reqUrl);
        }
    };
  }]);
