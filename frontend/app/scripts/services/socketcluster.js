'use strict';

/**
 * @ngdoc service
 * @name fjbchatApp.socketCluster
 * @description
 * # socketCluster
 * Service in the fjbchatApp.
 */
angular.module('fjbchatApp')
  .service('SocketCluster', ['Auth', 'Constant', 'Backend', function (Auth, Constant, Backend) {
    // AngularJS will instantiate a singleton by calling "new" on this function
    var socket, subscribedChannel, presenceChannel;
    return {
    	connect: function(watchChat, watchPresence) {
    		socket = socketCluster.connect({
                hostname: Constant.socketServerHost,
                port: Constant.socketServerPort
            });
            console.log(socket);
            socket.on('connect', function(res) {
                if(!res.isAuthenticated) {
                    console.log('not authenticated');
                    console.log(socket);
                    console.log('emit login');
                    socket.emit('login', Auth.getToken(), function(err){
                        if(err) {
                            console.log(err);
                        }
                        else {
                            console.log('login success');
                        }
                    });
                }
                else {
                    console.log('authenticated');
                    console.log(Auth.user());
                    console.log(socket);
                    console.log('subscribe : private:' + Auth.user().id);
                    subscribedChannel = socket.subscribe('private:' + Auth.user().id);
                    subscribedChannel.watch(watchChat);
                    console.log('subscribe : presence:all');
                    presenceChannel = socket.subscribe('presence:all');
                    presenceChannel.watch(watchPresence);
                }
            });
            socket.on('authenticate', function(token) {
                console.log('authenticated now, token :');
                console.log(token);
                console.log(Auth.user());
                console.log('subscribe : private:' + Auth.user().id);
                subscribedChannel = socket.subscribe('private:' + Auth.user().id);
                subscribedChannel.watch(watchChat);
                console.log('subscribe : presence:all');
                presenceChannel = socket.subscribe('presence:all');
                presenceChannel.watch(watchPresence);
            });
    	},
    	sendChat: function(targetChannel, data, callback) {
            console.log('publish to ' + targetChannel);
            console.log(data);
    		socket.publish(targetChannel, data, callback);
    	},
        getChatHistory: function(targetId, success, error) {
            console.log('get chat');
            console.log(targetId);
            Backend.get('chat/' + targetId, success, error);
        },
        logout: function() {
            console.log(socket);
            socket.emit('logout', function() {});
            socket.disconnect();
            console.log(socket);
        }
    };
  }]);