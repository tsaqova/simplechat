'use strict';

/**
 * @ngdoc service
 * @name fjbchatApp.user
 * @description
 * # user
 * Service in the fjbchatApp.
 */
angular.module('fjbchatApp')
  .service('User', ['$resource', 'Backend', function ($resource, Backend) {
    return $resource(Backend.url('user/:id'), {id: '@id'});
  }]);