var gulp = require('gulp');
var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var del = require('del');

var paths = {
	css: [
		'frontend/css/*.css'
	],
	js : [
		'frontend/js/*.js'
	],
	bowerjs : [
		'bower_components/jquery/dist/jquery.js',
		'bower_components/bootstrap/dist/js/bootstrap.js',
		'bower_components/angular/angular.js',
		'bower_components/angular-route/angular-route.js'
	],
	bowercss : [
		'bower_components/bootstrap/dist/css/bootstrap.css',
	]
};

gulp.task('cleanstyles', function(cb) {
	del(['public/css/styles.min.css'], cb);
});

gulp.task('cleanbowercss', function(cb) {
	del(['public/css/bowercss.min.css'], cb);
});

gulp.task('cleanbowerjs', function(cb) {
	del(['public/js/bowerjs.min.js'], cb);
});

gulp.task('cleanscripts', function(cb) {
	del(['public/js/scripts.min.js'], cb);
});

gulp.task('styles', ['cleanstyles'], function() {
	return gulp.src(paths.css)
			.pipe(concat('styles.min.css'))
			.pipe(gulp.dest('public/css'));
});

gulp.task('bowercss', ['cleanbowercss'], function() {
	return gulp.src(paths.bowercss)
			.pipe(concat('bowercss.min.css'))
			.pipe(gulp.dest('public/css'));
});

gulp.task('bowerjs', ['cleanbowerjs'], function() {
	return gulp.src(paths.bowerjs)
			.pipe(concat('bowerjs.min.js'))
			.pipe(gulp.dest('public/js'));
});

gulp.task('scripts', ['cleanscripts'], function() {
	return gulp.src(paths.js)
			.pipe(concat('scripts.min.js'))
			.pipe(gulp.dest('public/js'));
});

gulp.task('watch', function() {
	gulp.watch(paths.bowercss, ['bowercss']);
	gulp.watch(paths.js, ['scripts']);
	gulp.watch(paths.bowerjs, ['bowerjs']);
	gulp.watch(paths.bowerjs, ['styles']);
});

gulp.task('default', ['watch', 'bowercss', 'bowerjs', 'scripts', 'styles']);