'use strict';

/**
 * @ngdoc function
 * @name fjbchatApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the fjbchatApp
 */
 angular.module('fjbchatApp')
 .controller('MainCtrl', ['$localStorage', '$rootScope', '$scope', 'Auth', 'User', 'SocketCluster', function ($localStorage, $rootScope, $scope, Auth, User, SocketCluster) {
  Auth.reload();
  SocketCluster.connect(function(chat) {
    if(!$rootScope.chatBoxes[chat.keyID]) {
      SocketCluster.getChatHistory(chat.keyID, function(res, data) {
        var chatData = res.data;
        $rootScope.chatBoxes[chat.keyID] = $scope.initChatbox(chatData);
        $localStorage.chatBoxes = JSON.stringify($rootScope.chatBoxes);
      }, function(res) {
      });
    }
    else {
      $rootScope.chatBoxes[chat.keyID].chats.push(chat);
    }
  });

  if($localStorage.chatBoxes) {
    $rootScope.chatBoxes = JSON.parse($localStorage.chatBoxes);
  }
  else {
    $rootScope.chatBoxes = {};
  }

  $scope.users = User.query();

  $scope.initChatbox = function(data) {
    var chatBox = {
      title: data.title,
      channelName: data.channelName,
      targetId: data.targetId,
      chats: data.messages,
      display: true
    };
    return chatBox;
  }

  $scope.openChatBox = function(user) {
    if(!(user.id in $rootScope.chatBoxes)) {
      SocketCluster.getChatHistory(user.id, function(res, data) {
        var chatData = res.data;
        $rootScope.chatBoxes[user.id] = $scope.initChatbox(chatData);
        $localStorage.chatBoxes = JSON.stringify($rootScope.chatBoxes);
      }, function(res) {
      });
    }
  };

  $scope.closeChatBox = function(key) {
    delete $rootScope.chatBoxes[key];
    $localStorage.chatBoxes = JSON.stringify($rootScope.chatBoxes)
  };

  $scope.toggleChatBox = function(chatBox) {
    chatBox.display = !chatBox.display;
  }

  $scope.sendChat = function(chatBox, message) {
    var chat = {
      source: Auth.user().id,
      sourceName: Auth.user().name,
      target: chatBox.targetId,
      targetName: chatBox.title,
      message: message
    }
    console.log('send chat :');
    console.log(chat);
    SocketCluster.sendChat(chatBox.channelName, chat);
  }

  $scope.logout = function() {
    Auth.logout();
  }

}]);