'use strict';

describe('Controller: LoginCtrl', function () {

  // load the controller's module
  beforeEach(module('fjbchatApp'));

  var LoginCtrl,
    scope, Auth;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope, _Auth_) {
    scope = $rootScope.$new();
    LoginCtrl = $controller('LoginCtrl', {
      $scope: scope,
      Auth: _Auth_
      // place here mocked dependencies
    });
  }));

  it('should have username', function () {
    expect(scope.username).toBeDefined();
  });

  it('username has same length as input', function () {
    var input = 'testing';
    scope.username = input;
    expect(scope.username.length).toBe(input.length);
  });

  it('should have password', function () {
    expect(scope.password).toBeDefined();
  });

  it('password has same length as input', function () {
    var input = 'testing';
    scope.password = input;
    expect(scope.password.length).toBe(input.length);
  });

  it('should have submitLogin function', function () {
    expect(scope.submitLogin).toBeDefined();
  });

});
