'use strict';

describe('Controller: RegisterCtrl', function () {

  // load the controller's module
  beforeEach(module('fjbchatApp'));

  var RegisterCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    RegisterCtrl = $controller('RegisterCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should have name', function () {
    expect(scope.name).toBeDefined();
  });

  it('name has same length as input', function () {
    var input = 'testing';
    scope.name = input;
    expect(scope.name.length).toBe(input.length);
  });

  it('should have username', function () {
    expect(scope.username).toBeDefined();
  });

  it('username has same length as input', function () {
    var input = 'testing';
    scope.username = input;
    expect(scope.username.length).toBe(input.length);
  });

  it('should have password', function () {
    expect(scope.password).toBeDefined();
  });

  it('password has same length as input', function () {
    var input = 'testing';
    scope.password = input;
    expect(scope.password.length).toBe(input.length);
  });

  it('should have submitRegister function', function () {
    expect(scope.submitRegister).toBeDefined();
  });

});
