'use strict';

describe('Service: backend', function () {

  // load the service's module
  beforeEach(module('fjbchatApp'));

  // instantiate service
  var Backend, httpBackend, callBack;
  beforeEach(inject(function ($httpBackend, _Backend_) {
    Backend = _Backend_;
    httpBackend = $httpBackend;
    callBack = function() {
      // testing
    }
  }));

  afterEach(function() {
   // httpBackend.verifyNoOutstandingExpectation();
   // httpBackend.verifyNoOutstandingRequest();
  });

  it('should do get request and callback function called', function () {
    // httpBackend.expectGET('/testing');
    // Backend.get('/testing', callBack);
    // httpBackend.flush();
    // expect(callBack).toHaveBeenCalled();
  });

  it('should do post request and callback function called', function () {
    // httpBackend.expectPOST('/testing', null);
    // Backend.post('/testing', null, callBack)
    // httpBackend.flush();
    // expect(callBack).toHaveBeenCalled();
  });

  it('should return backend url', function() {
    // var url = Backend.url('/testing');
    // expect(url).toBeString();
  });

});
